﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeKeep : MonoBehaviour {

	public Text displayTime;
	public Text enterHours;
	public Text enterMinutes;
	public Text enterSeconds;
	public Text startStop;

	public Button stopAlarm;

	public InputField secondsDisplayField;
	public InputField minutesDisplayField;
	public InputField hoursDisplayField;

	private float timer = 0;
	private string hoursDisplay;
	private string minutesDisplay;
	private string secondsDisplay;
	private int minutes;

	private bool start = false;

	// Use this for initialization
	void Start () {
		stopAlarm.interactable = false;
	}
	
	// Update is called once per frame
	void Update () {

		if(start){
			timer -= Time.deltaTime; // I need timer which from a particular time goes to zero
			hoursDisplay = Mathf.Floor(timer / 3600).ToString("00");
			minutes = Mathf.FloorToInt (timer % 3600);
			minutesDisplay = (minutes / 60).ToString("00");
			secondsDisplay = (minutes % 60).ToString ("00");

			if (timer > 0)
			{
				//displayTime.text = "Time: " + hoursDisplay + ":" + minutesDisplay + ":" + secondsDisplay;
				hoursDisplayField.text = hoursDisplay + "h";
				minutesDisplayField.text = minutesDisplay + "m";
				secondsDisplayField.text = secondsDisplay + "s";
			} 
			else // timer is <= 0
			{
				hoursDisplayField.text = "00h";
				minutesDisplayField.text = "00m";
				secondsDisplayField.text = "00s";
				start = false;
				startStop.text = "Start";
				gameObject.GetComponent<AudioSource> ().Play ();
				stopAlarm.interactable = true;
			}
		}


	}

	public void EnterHours(){
		timer = (float) Functions.IntParseFast (enterHours.text) * 3600;
		minutesDisplayField.Select ();
	}
	public void EnterMinutes(){
		timer = (float) Functions.IntParseFast (enterMinutes.text) * 60 + timer;
		secondsDisplayField.Select ();
	}
	public void EnterSeconds(){
		timer = (float) Functions.IntParseFast (enterSeconds.text) + timer;
	}
	public void TimerStartStop(){
		start = !start;
		if (start) {
			startStop.text = "Stop";
		} else
			startStop.text = "Start";
	}
	public void TimerReset(){
		timer = 0;
		start = false;
		startStop.text = "Start";
		hoursDisplayField.text = "00h";
		minutesDisplayField.text = "00m";
		secondsDisplayField.text = "00s";
	}
	public void StopAlarm(){
		stopAlarm.interactable = false;
		gameObject.GetComponent<AudioSource> ().Stop ();
	}
}
