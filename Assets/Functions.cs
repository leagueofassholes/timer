﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Functions {

	public static int IntParseFast(string value)
	{
		int result = 0;
		for (int i = 0; i < value.Length; i++)
		{
			char letter = value[i];
			result = 10 * result + (letter - 48);
		}
		return result;
	}

	public static int IntParseFast(char value)
	{
		int result = 0;
		result = 10 * result + (value - 48);
		return result;
	}
}
