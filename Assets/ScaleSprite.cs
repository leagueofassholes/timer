﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleSprite : MonoBehaviour {
	public float worldScreenHeight, worldScreenWidth, xLeftBound, xRightBound, yLowBound, yHighBound, thisWidth, thisHeight;
	public Vector2 LLCorner, LRCorner, ULCorner, URCorner;
	private SpriteRenderer sr;

	private Vector3 designScaleW = Vector3.zero; //scale in world space
	private float designSizeW = 0f; // size of the sprite in x in world space.
	private float designScaleV = 0f; // viewport size of the sprite- percent of x that the sprite takes.
	public Vector2 designScreen = new Vector2(1280f,800); // the pixel size of the screen
	private float designScreenWidth = 0f; // the size of the width of the screen in units.    
	private float wScreenWidth = 0f; //how many units wide the screen is- "w" means world space.
	private float wScreenHeight = 0f; //how many units tall the screen is

	void Start()
	{


	}

	void Update()
	{

	}

	public void ScaleMe(){

		//Calculate Useful Variables for other Scripts
		worldScreenHeight = Camera.main.orthographicSize * 2;
		worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
		xLeftBound=Camera.main.transform.position.x - (worldScreenWidth/2);
		xRightBound=Camera.main.transform.position.x + (worldScreenWidth/2);
		yLowBound=Camera.main.transform.position.y - (worldScreenHeight/2);
		yHighBound=Camera.main.transform.position.y + (worldScreenHeight/2);
		LLCorner=new Vector2(xLeftBound,yLowBound);
		LRCorner= new Vector2(xRightBound,yLowBound);
		ULCorner=new Vector2(xLeftBound,yHighBound);
		URCorner=new Vector2(xRightBound,yHighBound);

		//now start scaling sprite
		sr = GetComponent<SpriteRenderer>();
		if (sr==null){
			sr = GetComponentInParent<SpriteRenderer> ();
		}
		designScaleW = transform.localScale; //save the original scale.
		designSizeW = sr.sprite.bounds.size.x * designScaleW.x; //save the original size, at the original scale.
		designScreenWidth = (Camera.main.orthographicSize*2f)/designScreen.y * designScreen.x; //get the width of the design screen
		designScaleV = designSizeW/designScreenWidth; //get the design viewport size. this is the target for the new resolution.

		//set scale
		wScreenHeight = Camera.main.orthographicSize * 2f; // calculation for how many units tall the screen is.
		wScreenWidth = wScreenHeight/Screen.height*Screen.width; // calculation for how many units wide the screen is.

		//We use the amount of space it took up in design to calculate what the scale needs to be now.
		float desiredSize = designScaleV * wScreenWidth; //The desired size is the design viewport size times the width of the screen in world units
		float xScale = desiredSize/sr.sprite.bounds.size.x; //calculate the scale in the x axis.

		float yScale = (xScale - designScaleW.x) + designScaleW.y; //calculate the scale in the y axis.
		Vector3 spriteScale = new Vector3(xScale, yScale, 1);
		transform.localScale = spriteScale; //Set the scale!
	
		thisWidth = sr.bounds.size.x;
		thisHeight = sr.bounds.size.y;
	}
}
